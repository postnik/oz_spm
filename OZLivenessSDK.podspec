Pod::Spec.new do |s|
  s.name = 'OZLivenessSDK'
  s.version = '8.7.0'
  s.summary = 'OZLivenessSDK'
  s.homepage = 'https://doc.ozforensics.com/oz-knowledge/guides/developer-guide/sdk/oz-mobile-sdk'
  s.authors = { 'oz-forensics' => 'info@ozforensics.com' }
  s.source = { :git => 'https://gitlab.com/postnik/oz_spm', tag: "#{s.version}" }
  s.ios.deployment_target  = '11.0'
  s.default_subspec = 'Full'
  
  s.swift_versions = ['4.2', '5.0']
  
  s.license = { type: 'Commercial', text: '© 2023 OZForensics. All rights reserved.\n' }
  
  s.static_framework = true
  s.pod_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
  
  s.subspec 'Full' do |ss|
    ss.vendored_frameworks = [
      'OZLivenessSDK.xcframework',
      'SDKResources.xcframework',
      'SDKOnDeviceResources.xcframework'
    ]
  end
  
  s.subspec 'Core' do |ss|
    ss.vendored_frameworks = [
      'OZLivenessSDK.xcframework',
      'SDKResources.xcframework',
      'SDKOnDeviceResources.xcframework'
    ]
  end
  
end

