//
//  OZLivenessSDK.h
//
//  Created by Ilya Postanogov on 02.05.2024.
//  Copyright © 2024 Oz Forensics. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreResources.
FOUNDATION_EXPORT double OZLivenessSDKVersionNumber;

//! Project version string for CoreResources.
FOUNDATION_EXPORT const unsigned char OZLivenessSDKVersionString[];

#import <OZLivenessSDK/ResourcesInterop.h>
