// swift-tools-version: 5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "OZLivenessSDK",
    defaultLocalization: "en",
    platforms: [.iOS(.v11)],
    products: [
        .library(name: "OZLivenessSDK",
                 targets: ["OZLivenessSDK", "SDKResources"]),
        .library(name: "OZLivenessSDKOnDeviceResources",
                 targets: ["SDKOnDeviceResources"]),
    ],
    targets: [
        .binaryTarget(name: "OZLivenessSDK", path: "./OZLivenessSDK.xcframework"),
        .binaryTarget(name: "SDKResources", path: "./SDKResources.xcframework"),
        .binaryTarget(name: "SDKOnDeviceResources", path: "./SDKOnDeviceResources.xcframework"),
    ],
    swiftLanguageVersions: [.v5]
)
